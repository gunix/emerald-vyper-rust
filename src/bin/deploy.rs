use ethers::prelude::*;
use ethers_contract::ContractFactory;
use ethers_core::abi::Abi;
use eyre::Result;
use std::fs;
use std::{convert::TryFrom, sync::Arc, time::Duration};

#[tokio::main]
async fn main() -> Result<()> {
    let emerald_ip = std::env::var("EMERALD_IP").unwrap_or_else(|_| "127.0.0.1".into());
    let emerald_uri = format!("http://{}:8545", emerald_ip);
    let private_key = std::env::var("PRIVATE_KEY").expect("PRIVATE_KEY env var is mandatory");
    let private_key = private_key.trim_start_matches("0x");

    let provider = Provider::<Http>::try_from(emerald_uri)?.interval(Duration::from_secs(7));
    let chain_id = provider.get_chainid().await.unwrap().as_u64();

    let wallet = private_key.parse::<LocalWallet>()?.with_chain_id(chain_id);
    println!("wallet public address is {:?}", wallet.address());

    let client = Arc::new(SignerMiddleware::new(provider, wallet));

    let abi: Abi =
        serde_json::from_str(&fs::read_to_string("vyper/target/greeter_abi.json").unwrap())?;

    let bytecode_string = fs::read_to_string("vyper/target/greeter_bytecode")?;
    let bytecode_string = bytecode_string.trim_end().trim_start_matches("0x");
    let bytecode = hex::decode(bytecode_string)?;
    let bytecode = ethers_core::types::Bytes::from(bytecode);

    let factory = ContractFactory::new(abi, bytecode, client);

    let contract = factory
        .deploy("HELLO FROM RUST!".to_string())?
        .legacy()
        .confirmations(0usize)
        .send()
        .await?;
    println!("Contract address: {:?}", contract.address());

    Ok(())
}
