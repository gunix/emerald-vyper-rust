use ethers::prelude::*;
use eyre::Result;
use std::{convert::TryFrom, sync::Arc, time::Duration};

abigen!(
    Greeter,
    "vyper/target/greeter_abi.json",
    event_derives(serde::Deserialize, serde::Serialize)
);

#[tokio::main]
async fn main() -> Result<()> {
    let emerald_ip = std::env::var("EMERALD_IP").unwrap_or_else(|_| "127.0.0.1".into());
    let emerald_uri = format!("http://{}:8545", emerald_ip);
    let private_key = std::env::var("PRIVATE_KEY").expect("PRIVATE_KEY env var is mandatory");
    let private_key = private_key.trim_start_matches("0x");
    let contract_address = std::env::var("CONTRACT_ADDRESS").expect("CONTRACT_ADDRESS env var is mandatory");

    let provider =
        Provider::<Http>::try_from(emerald_uri)?.interval(Duration::from_secs(7));
    let chain_id = provider.get_chainid().await.unwrap().as_u64();

    let wallet = private_key
        .parse::<LocalWallet>()?
        .with_chain_id(chain_id);
    println!("wallet public address is {:?}", wallet.address());

    let client = Arc::new(SignerMiddleware::new(provider, wallet));

    let contract_address = contract_address.parse::<Address>()?;
    let greeter_contract = Greeter::new(contract_address, client.clone());

    let greeting = greeter_contract.greet().call().await.unwrap();
    println!("The initial greeting of the contract is: {}", greeting);

    let result = greeter_contract
        .set_greeting("THE GREETING GOT CHANGED FROM RUST!".to_owned())
        .legacy()
        .send()
        .await?
        .await?;
    println!("The result of set_greeting is: {:?}", result);

    let greeting = greeter_contract.greet().call().await.unwrap();
    println!("The new greeting of the contract is: {}", greeting);

    Ok(())
}

