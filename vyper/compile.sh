#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
TARGET_DIR="${SCRIPT_DIR}/target"

# compile the vyper code
mkdir -p "${TARGET_DIR}"
~/.local/pipx/venvs/vyper/bin/vyper -f abi "${SCRIPT_DIR}/greeter.vy" | jq > "${TARGET_DIR}/greeter_abi.json"
~/.local/pipx/venvs/vyper/bin/vyper -f bytecode "${SCRIPT_DIR}/greeter.vy" > "${TARGET_DIR}/greeter_bytecode"

# generate the greeter JSON based on the format expected by abigen!
# more details here:
# https://github.com/gakonst/ethers-rs/blob/master/examples/contract_with_abi_and_bytecode.rs#L5-L9
{
echo -n '{"abi": '
cat "${TARGET_DIR}/greeter_abi.json"
echo -n ', "bin": "'
cat "${TARGET_DIR}/greeter_bytecode" | tr -d '\n'
echo '"}'
} | jq > "${TARGET_DIR}/greeter_artifact.json"
