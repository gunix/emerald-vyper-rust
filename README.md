# Emerald + Vyper + Rust

This repo is an example of how to deploy and call vyper smart contracts on [Emerald](https://docs.oasis.dev/general/developer-resources/emerald-paratime/) by using Rust. You will not need any of these technologies: javascript, typescript, hardhat, truffle, remix. All you need is vyper and rust.

### Dependencies & Tech Stack

Please find bellow the tech stack used to build the project:

- [emerald all-in-one container](https://docs.oasis.dev/general/developer-resources/emerald-paratime/writing-dapps-on-emerald#running-private-oasis-network-locally)
- [vyper](https://vyper.readthedocs.io/en/stable/) (you should install it through `pipx install vyper`)
- rust development setup - if you are a vim user, maybe try [this](https://codeberg.org/gunix/nvim-rust-ide/)
- [ethers-rs](https://docs.rs/ethers/latest/ethers/) - this will get pulled in by cargo
- (optional) jq - I used it in a script to prettify JSONs

### Running the demo

Compile the contract:
```bash
./vyper/compile.sh
```

Start your local emerald cluster:
```bash
podman run -it -p8545:8545 -p8546:8546 docker.io/oasisprotocol/emerald-dev:latest
```

If running the emerald blockchain on another machine, export the `EMERALD_IP` env var:
```bash
export EMERALD_IP=192.168.122.1
```

Export your private key (if running locally, you will find it in the container log):
```bash
export PRIVATE_KEY=0xc7f215d668759f658fa197944841a206bd04830fb8f39b727e7ce538f441b8f8
```

Deploy contract `cargo run --bin deploy`:
```bash
➤ cargo run --bin deploy
   Compiling emerald-demo v0.1.0 (/root/git/emerald-vyper-rust)
    Finished dev [unoptimized + debuginfo] target(s) in 4.66s
     Running `target/debug/deploy`
wallet public address is 0x6582513b7f4fd9188ca562c6756d51cd31780a5d
Contract address: 0x25c13e9a6f609acbf808010ac92f69c9f9a28703
```
You will need the contract address in the next opeartion.

Call the contract: `CONTRACT_ADDRESS=0xd46045d030eea0369cbb3060f4d309b41048d0a1 cargo run --bin call`
```bash
➤ CONTRACT_ADDRESS=0xd46045d030eea0369cbb3060f4d309b41048d0a1 cargo run --bin call
    Finished dev [unoptimized + debuginfo] target(s) in 0.09s
     Running `target/debug/call`
wallet public address is 0x5e1834613d0b3e94d774c5df945b2d6943d12c2e
The initial greeting of the contract is: HELLO FROM VYPER! HELLO FROM RUST!
The result of set_greeting is: Some(TransactionReceipt { transaction_hash: 0x3ffa72b461edca145113c0e61edd5a49941ae2be6452969a58924cdd08491d1e, transaction_index: 0, block_hash: Some(0xdcb76446e0e499d7b75eeaa32fef005973f282997be8c0ec87bb8dae3a3c1398), block_number: Some(100), cumulative_gas_used: 30283, gas_used: Some(30283), contract_address: None, logs: [], status: Some(1), root: None, logs_bloom: 0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, transaction_type: Some(0), effective_gas_price: None })
The new greeting of the contract is: THE GREETING GOT CHANGED FROM RUST!
```

## Deploy Vyper dApp to Emerald with Rust

Though Solidity is usually the most popular choice when it comes to smart contracts,
[vyper](https://vyper.readthedocs.io/en/stable/) is also a modern and popular alternative.
As Emerald provides full EVM support, Vyper is a viable choice for your Emerald dApps.
Apart from the choice of programming language when it comes to smart contracts, you also
are free to pick alternatives of JavaScript/TypeScript when it comes to deploying or
calling your dApps. For example, you can also use the
[Rust version of ethers](https://github.com/gakonst/ethers-rs/) to deploy your dApps to
Emerald.

Let's consider the following example of a small Vyper smart contract:
```python
greeting: String[100]
@external
def __init__(_greeting: String[80]):
  self.greeting = concat("HELLO FROM VYPER! ", _greeting)
@external
def greet() -> String[100]:
  return self.greeting
@external
def setGreeting(_greeting: String[100]):
  self.greeting = _greeting
```

If this is your first experience with Vyper, you must first install the compiler. The
best practice is to install it via [pipx](https://pypi.org/project/pipx/):
`pipx install vyper`, so that it runs from an isolated environment. After this, you
can compile your ABI and your bytecode by running the following bash commands:

```bash
# considering you save the smart contract to greeter.vy, this will compile the abi:
~/.local/pipx/venvs/vyper/bin/vyper -f abi greeter.vy > greeter_abi.json
# and this command will allow you to compile the bytecode:
~/.local/pipx/venvs/vyper/bin/vyper -f bytecode greeter.vy > greeter_bytecode
```

The ABI contains an instruction set that you can import in any client that will call
your smart contract, and the bytecode contains the actual contract that will get
executed by the blockchain.

Now you that have these two files, you can proceed to deploy.
If you would like to stick to Hardhat, you first have to install
[the plugin](https://hardhat.org/plugins/nomiclabs-hardhat-vyper.html) and after that
you can proceed with the normal usage of Hardhat. In case you are looking for alternatives,
please find below an example of how to deploy the vyper smart contract by using Rust:

```rust
use ethers::prelude::*;
use ethers_contract::ContractFactory;
use ethers_core::abi::Abi;
use eyre::Result;
use std::fs;
use std::{convert::TryFrom, sync::Arc, time::Duration};
#[tokio::main]
async fn main() -> Result<()> {
    let emerald_ip = std::env::var("EMERALD_IP").unwrap_or_else(|_| "127.0.0.1".into());
    let emerald_uri = format!("http://{}:8545", emerald_ip);
    let private_key = std::env::var("PRIVATE_KEY").expect("PRIVATE_KEY env var is mandatory");
    let private_key = private_key.trim_start_matches("0x");
    let provider = Provider::<Http>::try_from(emerald_uri)?.interval(Duration::from_secs(7));
    let chain_id = provider.get_chainid().await.unwrap().as_u64();
    let wallet = private_key.parse::<LocalWallet>()?.with_chain_id(chain_id);
    println!("wallet public address is {:?}", wallet.address());
    let client = Arc::new(SignerMiddleware::new(provider, wallet));
    let abi: Abi =
        serde_json::from_str(&fs::read_to_string("path/to/greeter_abi.json").unwrap())?;
    let bytecode_string = fs::read_to_string("path/to/greeter_bytecode")?;
    let bytecode_string = bytecode_string.trim_end().trim_start_matches("0x");
    let bytecode = hex::decode(bytecode_string)?;
    let bytecode = ethers_core::types::Bytes::from(bytecode);
    let factory = ContractFactory::new(abi, bytecode, client);
    let contract = factory
        .deploy("HELLO FROM RUST!".to_string())?
        .legacy()
        .confirmations(0usize)
        .send()
        .await?;
    println!("Contract address: {:?}", contract.address());
    Ok(())
}
```
Depending on your preference, you should be able to use any technology that
you normally use with EVM when you use Emerald. The chain was designed to offer
backwards compatibility for existing Ethereum dApps on a modern and performant
environment. For example, another very popular choice is
[go-ethereum](https://github.com/ethereum/go-ethereum/).

